##########################
# michaellvaknin
##########################

# Exercise 1

# Write a program that gets two strings from the user: one for a name and the other for a password. If the login information (name and password) matches one of the users in the table below, "Welcome Master" must be printed, otherwise "INTRUDER ALERT" must be printed. The correct login details are:

# Name: apple, Password: red
# Name: lettuce, Password: green
# Name: lemon, Password: yellow
# Name: orange, Password: orange
#########

usernames_passwords = {"apple":"red","lettuce":"green","lemon":"yellow","orange":"orange"}

def login():
   username=input("Username: ")
   password=input("Password: ")
   if username not in usernames_passwords:
      print("INTRUDER ALERT")
   elif usernames_passwords[username]==password:
      print("Welcome Master")
   elif usernames_passwords[username] !=password:
      print("INTRUDER ALERT")
   
login()

#########
# Exercise 2

# Write a Python program that receives a list of 20 scores through the command line. The program must calculate the grade point average and print all grades higher than average. For example:

# python 02.py 99 90 15 28 38 44 50 81 79 60 99 90 15 28 38 44 50 81 79 60

# printout:

# 99 90 81 79 60 99 90 81 79 60

# Hint: The special sys.argv list represents all the parameters passed to Python via the command line. Try to find it and understand how to use it to solve the exercise.
#########

import sys
mlist = []
value = sum(map(float, sys.argv[1:]))
num = len(sys.argv[1:])
arg = value/num
print("the average scores: ", arg)

for i in range(1, num):
    if float(sys.argv[i]) > arg:
        mlist.append(float(sys.argv[i]))
print("the bigest grades: ", mlist)

#########
# Exercise 3

# Given a file named hosts that contains rows of the form hostname = ipaddress. For example, the contents of such a file may look like this:

# work = 10.0.0.2
# router = 10.0.0.1
# mycar = 10.0.0.5
# home = 194.90.2.1

# Write a program that receives from the user a list of computer names as parameters from the command line and prints all the IP addresses of the computers in the list. If one of the names does not appear in the file, an appropriate error message must be displayed.
#########

hosts = {}

while True:
   host = input("please provide host: ")
   ip = input("please provide ip: ")
   if len(host) == 0:
      break
   hosts[host] = ip
   for i in hosts:
      print(f"{i}={hosts[i]}")
#########
# Exercise 4

# Write a program that identifies anagrams in a list of words: Two words are an anagram if they have the same letters in a different order. The program will get a path to the file containing a list of words and print all the words that are anagrams of each other in the same line.

# For example if the file has the following lines:

# add
# dad
# help
# more
# rome

# The program must print:

# add dad
# help
# more rome
#########

listN = []
alternatives = ['car','acr','dad','add','dcdd','cdc','bbc','dccds','cbb']
for i in alternatives:
    for alt in alternatives:
        if i != alt:
            if sorted(i) == sorted(alt):
                alternatives.remove(alt)
                word = i + " " + alt
                listN.append(word)
    alternatives.remove(i)
for j in alternatives:
    listN.append(j)
print(listN)


#########