#!/usr/bin/env python3
# . תרגיל 1

# הקוד הבא מניח קיום מחלקה בשם Summer. השלימו את קוד המחלקה כדי שהקוד ידפיס את התוצאה הנכונה:

# s = Summer()
# t = Summer()

# s.add(10, 20)
# t.add(50)
# s.add(30)

# # should print 60
# s.print_total()

# # should print 50
# t.print_total()
####

class Summer:

    def __init__(self):
        self.nums = 0
    def add(self, *args):
        for i in args:
            self.nums += i
    def print_total(self):
        print(self.nums)

s = Summer()
t = Summer() 
s.add(10, 20)
t.add(50)
s.add(30)
s.print_total()
t.print_total()

####
# 2. תרגיל 2

# הקוד הבא מניח קיום מחלקה בשם MyCounter. השלימו את קוד המחלקה כדי שהקוד ידפיס את התוצאה הנכונה.

# for _ in range(10):
#      c1 = MyCounter()

# # should print 10
# print(MyCounter.count)
####

class MyCounter:
    count = 0
    def __init__(self):
        self.__class__.count += 1

for _ in range(10):
     c1 = MyCounter()

print(MyCounter.count)

####
# 3. תרגיל 3

# הקוד הבא מניח קיום מחלקה בשם Widget המייצגת רכיב שצריך להיבנות. כל רכיב עשוי להיות תלוי ברכיבים אחרים. בניית רכיב שתלוי ברכיבים אחרים תבנה אוטומטית גם את כל הרכיבים בהם הוא תלוי. אין צורך לבנות רכיב פעמיים. כתבו את המחלקה Widget כך שהקוד הבא יעבוד:

# luke    = Widget("Luke")
# hansolo = Widget("Han Solo")
# leia    = Widget("Leia")
# yoda    = Widget("Yoda")
# padme   = Widget("Padme Amidala")
# anakin  = Widget("Anakin Skywalker")
# obi     = Widget("Obi-Wan")
# darth   = Widget("Darth Vader")
# _all    = Widget("")


# luke.add_dependency(hansolo, leia, yoda)
# leia.add_dependency(padme, anakin)
# obi.add_dependency(yoda)
# darth.add_dependency(anakin)

# _all.add_dependency(luke, hansolo, leia, yoda, padme, anakin, obi, darth)
# _all.build()
# # code should print: Han Solo, Padme Amidala, Anakin Skywalker, Leia, Yoda, Luke, Obi-Wan, Darth Vader
# # (can print with newlines in between modules)
####

#!/usr/bin/python3
class Widget:
    def __init__(self, name):
        self.name = name 
        self.dependency = []
        self.built = False
    def add_dependency(self, *dependency):
        self.dependency.extend(dependency)
    def build(self):
        self.built = True
        for d in self.dependency:
            if not d.built:
                d.build()
                print(d.name)
                
luke    = Widget("Luke")
hansolo = Widget("Han Solo")
leia    = Widget("Leia")
yoda    = Widget("Yoda")
padme   = Widget("Padme Amidala")
anakin  = Widget("Anakin Skywalker")
obi     = Widget("Obi-Wan")
darth   = Widget("Darth Vader")
_all    = Widget("")


luke.add_dependency(hansolo, leia, yoda)
leia.add_dependency(padme, anakin)
obi.add_dependency(yoda)
darth.add_dependency(anakin)

_all.add_dependency(luke, hansolo, leia, yoda, padme, anakin, obi, darth)
_all.build()

####