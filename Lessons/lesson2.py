#!/usr/bin/env python3

#michaellvaknin

#כתבו תוכנית המקבלת 10 מספרים מהמשתמש ומדפיסה את הגדול ביותר.

nums = []
for i in range(10):
    nums += [int(input("Enter number: "))]
print("the highest number is: ", max(nums))

#כתבו תוכנית המבקשת מהמשתמש להכניס את גילו ומדפיסה חזרה את הגיל בחודשים. אם המשתמש הכניס גיל שאינו מספר יש להציג הודעת שגיאה ולבקש שוב.

while True:
    age = input(' What is your age? ')
    if age.isnumeric():
        age = int(age)
        print(f"{age*12}") 
        break
    else:
        print('enter numbers!')

#כתבו תוכנית הקוראת שורות מהמשתמש עד שהמשתמש מכניס שורה ריקה. לאחר הכנסת שורה ריקה יש להדפיס חזרה למשתמש את כל השורות שכתב מהסוף להתחלה.

word_list =[]
while True:
    inp = input("enter your words: ")
    if inp == '':
        print(word_list[::-1])
        break 
    else:
       word_list.append(inp)

#כתבו תוכנית המגרילה בלולאה מספרים שלמים בין 1 ל 1,000,000 עד שמוצאת מספר המתחלק גם ב-7, גם ב-13 וגם ב-15.

import random
num = 1
while(num % 7 ) and (num % 13) and (num % 15):
    num = random.randint(1,1000000)
print(num)

#כתבו תוכנית המגרילה שני מספרים בין 1 ל-10 ומחשבת את המכפלה המשותפת הקטנה ביותר שלהם, כלומר המספר הקטן ביותר שמתחלק בשני המספרים. אם לדוגמא הגרלתם את המספרים 4 ו-6 יש להדפיס חזרה את המספר 12.
from random import randint

def Compute_LCM(n1,n2):
    if n1>n2:
        higher = n1
    else:
        higher = n2
    value = higher
    while True:
        if higher%n1==0 and higher%n2==0:
            print("LCM of" ,n1, "and" ,n2,"is" , higher)
            break
        else:
            higher = higher+value
n1 = (randint(1, 10))
n2 = (randint(1, 10))
Compute_LCM(n1,n2)


#כתבו תוכנית הבוחרת באקראי מספר בין 1 ל-100. על המשתמש לנחש את המספר שנבחר ואחרי כל ניחוש יש להדפיס ״גדול מדי״ או ״קטן מדי״ לפי היחס בין המספר שנבחר לניחוש. בונוס: כדי שיהיה מעניין דאגו שמדי פעם התוכנית תדפיס את ההודעה הלא נכונה.

import random

number = random.randrange(1,100)
guess = int(input("guess a number between 1 and 100: "))

while guess != number:
    if guess < number:
        print("you need to guess higer. Try again")
        guess = int(input("\nguess a number between 1 and 100: "))
    else:
        print("you need to guess lower. Try again ")
        guess = int(input("\nguess a number between 1 and 100: "))
print("you are the best")
print ("You win in {0} tries".format(guess))

#
