#!/usr/bin/env python3


#Lesson on inheritance

class Person:
    def __init__(self, name, lname):
        self.name = name
        self.lname = lname
    def printname(self):
        print(self.name, self.lname)

class Robot:
    def __init__(self, name="", is_alive=False):
        self.name = name
        self.is_alive = is_alive
 
class Animal:
    def __init__(self, name="", is_alive=True):
        self.name = name
        self.is_alive = is_alive

class Oldperson(Person):
    def __init__(self,name, lname, age):
        Person.__init__(self,name,lname)
        self.age = age
    def printage(self,age):
        self.age = age
        return self.age


shmulik = Oldperson('shmulik', 'shatz', 101)
print(shmulik.age)
print(shmulik.name)
print(shmulik.lname)
